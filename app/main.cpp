#include <iostream>
#include "student.h"
using namespace std;
int main()
{
    Student a("123_ab", 21, 2.1);
    a.set_subject_marks("Science", 30);
    int mark = a.get_subject_marks("maths");
    if (mark == -1)
    {
        cout << "No such Subject existed" << endl;
    }
    else
    {
        cout << mark;
    }
    a.set_subject_marks("Maths", 40);
    a.print_all_marks();
    return 0;
}