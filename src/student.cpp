#include <iostream>
#include "../include/student.h"
#include <map>
#include <string>

Student::Student(string roll_no, int age, float cgpa)
{
    self.roll_no = roll_no;
    self.age = age;
    self.cgpa = cgpa;
}

void Student::set_subject_marks(string subject, int marks)
{
    resultMap[subject] = marks;
}

int Student::get_subject_marks(string subject)
{
    map<string, int>::iterator itr = resultMap.find(subject);
    if (itr != resultMap.end())
    {
        return itr->second;
    }
    else
    {
        return -1;
    }
}

void Student::print_all_marks()
{
    map<string, int>::iterator itr;
    if (resultMap.begin() == resultMap.end())
    {
        cout << "No subject is registered";
    }
    else
    {
        for (itr = resultMap.begin(); itr != resultMap.end(); itr++)
        {
            cout << "Subject: " << (*itr).first << " Marks: " << (*itr).second << "\n";
        }
    }
}

Student::~Student()
{
    cout << "Destructor is called" << endl;
}
