cmake_minimum_required(VERSION 3.16.3)
project(CmakeTask)
add_subdirectory(app)
add_subdirectory(include)